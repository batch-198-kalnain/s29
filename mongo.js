// db.products.insertMany(
//     [
//         {
//             "name":"Iphone X",
//             "price":30000,
//             "isActive":true
//         },
//         {
//             "name":"Samsung Galaxy S21",
//             "price":51000,
//             "isActive":true
//         },
//         {
//             "name":"Razer Blackshark V2X",
//             "price":2800,
//             "isActive":false
//         },
//         {
//             "name":"RAKK Gaming Mouse",
//             "price":1800,
//             "isActive":true
//         },
//         {
//             "name":"Razer Mechanical Keyboard",
//             "price":4000,
//             "isActive":true
//          }
//     ]
// )


  ///////////////////
 // Query Operator//
///////////////////
// Allows us to expand our queries and define condition instead of just looking specific value.

// $gt, $lt, $gte, $lte

// db.products.find({"price":{$gt:3000}})
// db.products.find({"price":{$lt:3000}})
// db.products.find({"price":{$gte:30000}})
// db.products.find({"price":{$lte:2800}})

// db.users.insertMany(
//     [
//         {
//             firstname:"Mary Jane",
//             lastname:"Watson",
//             email:"mjtiger@gmail.com",
//             password:"tigerjackpot15",
//             isAdmin:false
//         },
//         {
//             firstname:"Gwen",
//             lastname:"Stacy",
//             email:"stacyTech@gmail.com",
//             password:"stacyTech1991",
//             isAdmin:true  
//         },
//         {
//             firstname:"Peter",
//             lastname:"Parker",
//             email:"peterWebDev@gmail.com",
//             password:"webdeveloperPeter",
//             isAdmin:true
//         },
//         {
//             firstname:"Jonah",
//             lastname:"Jameson",
//             email:"jjjameson@gmail.com",
//             password:"spideyisamenace",
//             isAdmin:false
//         },
//         {
//             firstname:"Otto",
//             lastname:"Octavius",
//             email:"ottoOctapi@gmail.com",
//             password:"docOck15",
//             isAdmin:true
//         }
//     ]
// )

  //////////////
 //  $regex  //
//////////////

//regex query operator which allow us to find documents which will match the characters/pattern of the characters we are looking for.

// $regex looks for documents with partial match and by default is case sensitive.
// db.users.find({firstname:{$regex:'O'}})

  //////////////
 // $options //
////////////// 
// used our regex will be case insensitive
// db.users.find({firstname:{$regex:'o',$options:'i'}})

// It can also find for document with partial mathces
// db.products.find({"name":{$regex:'phone',$options:'i'}})

// Find users whose email have the word "web" in it
// db.users.find({email:{$regex:'web',$options:'$i'}})

// db.products.find({name:{$regex:'razer',$options:'$i'}})
// db.products.find({"name":{$regex:'rakk',$options:'i'}})

  ///////////////////
 //  $or and $and //
///////////////////
// Works almost the same way as they in JS.

// ex. of $or
// db.products.find({$or:[{"name":{$regex:'x',$options:'$i'}},{"price":{$lte:10000}}]})
// db.products.find({$or:[{"name":{$regex:'x',$options:'$i'}},{"price":{$gte:30000}}]})

// ex. of $and
// db.products.find({$and:[{"name": {$regex:'razer',$options:'$i'}},{"price":{$gte:3000}}]})
// db.products.find({$and:[{"name":{$regex:'x',$options:'$i'}},{"price":{$gte:30000}}]})
// db.users.find({$and:[{lastname:{$regex:'w',$options:'$i'}},{isAdmin:false}]})
// db.users.find({$and:[{firstname:{$regex:'a',$options:'$i'}},{isAdmin:true}]})


  //////////////////////
 // Field Projection //
//////////////////////

// Will allow us to show/hide certain properties/fields
// db.collection.find({query},{projection}) 
// db.users.find({},{_id:0,password:0})

// Email will be the only one to show
// db.users.find({isAdmin:true},{_id:0,email:1})

// db.users.find({isAdmin:false},{firstname:1,lastname:1})
// db.products.find({"price":{$gte:1000}},{_id:0,"name":1,"price":1})